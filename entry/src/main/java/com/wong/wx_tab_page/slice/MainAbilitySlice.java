package com.wong.wx_tab_page.slice;

import com.google.common.collect.Lists;
import com.qincji.bottombar.TabBar;
import com.qincji.bottombar.TabItem;
import com.qincji.bottombar.TabUtils;
import com.wong.wx_tab_page.ResourceTable;
import com.wong.wx_tab_page.provider.HistPageSliderProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;
import ohos.agp.utils.Color;

import java.util.List;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        initTabPageSlider();
    }

    private void initTabPageSlider() {
        // 初始化基础页面
        List<Integer> list = Lists.newArrayList();
        list.add(ResourceTable.Layout_ability_main_index);
        list.add(ResourceTable.Layout_ability_main_poet);
        list.add(ResourceTable.Layout_ability_main_coll);
        list.add(ResourceTable.Layout_ability_main_mine);
        HistPageSliderProvider provider = new HistPageSliderProvider(list, this);
        PageSlider pageSlider = findComponentById(ResourceTable.Id_hist_page_slice);
        pageSlider.setProvider(provider);
        pageSlider.setSlidingPossible(false);


        // 初始化底部菜单
        TabBar tabBar = findComponentById(ResourceTable.Id_tab_bar);
        TabItem indexTab = new TabItem()
                .imgNormalId(ResourceTable.Graphic_background_main_index_normal)
                .imgActiveId(ResourceTable.Graphic_background_main_index_actived)
                .desc("首页")
                .textNormalColor(new Color(Color.getIntColor("#666666")))
                .textActiveColor(new Color(Color.getIntColor("#0074FC")))
                .textSize(TabUtils.vp2px(this, 14));
        TabItem poetTab = new TabItem()
                .imgNormalId(ResourceTable.Graphic_background_main_poet_normal)
                .imgActiveId(ResourceTable.Graphic_background_main_poet_actived)
                .desc("诗词")
                .textNormalColor(new Color(Color.getIntColor("#666666")))
                .textActiveColor(new Color(Color.getIntColor("#0074FC")))
                .textSize(TabUtils.vp2px(this, 14));
        TabItem collTab = new TabItem()
                .imgNormalId(ResourceTable.Graphic_background_main_coll_normal)
                .imgActiveId(ResourceTable.Graphic_background_main_coll_actived)
                .desc("收藏")
                .textNormalColor(new Color(Color.getIntColor("#666666")))
                .textActiveColor(new Color(Color.getIntColor("#0074FC")))
                .textSize(TabUtils.vp2px(this, 14));
        TabItem mineTab = new TabItem()
                .imgNormalId(ResourceTable.Graphic_background_main_mine_normal)
                .imgActiveId(ResourceTable.Graphic_background_main_mine_actived)
                .desc("我的")
                .textNormalColor(new Color(Color.getIntColor("#666666")))
                .textActiveColor(new Color(Color.getIntColor("#0074FC")))
                .textSize(TabUtils.vp2px(this, 14));
        tabBar.init(indexTab, poetTab, collTab, mineTab)
                .setOnTabBarClickListener((tab, layout, item) -> {
                    pageSlider.setCurrentPage(tab);
                }).create();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
